package com.nimbusds.openid.connect.provider.spi.grants.password.webapi;


import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Optional;

import static net.jadler.Jadler.*;
import static org.junit.Assert.*;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nimbusds.common.contenttype.ContentType;
import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.grants.AccessTokenSpec;
import com.nimbusds.openid.connect.provider.spi.grants.PasswordGrantAuthorization;
import com.nimbusds.openid.connect.provider.spi.grants.RefreshTokenSpec;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;


/**
 * Tests the password grant API against a mock HTTP server.
 */
public class PasswordGrantWebAPITest {


	private static class HandlerRequestMatcher extends BaseMatcher<String> {

		private final ResourceOwnerPasswordCredentialsGrant grant;

		private final ClientID clientID;

		private final OIDCClientMetadata clientMetadata;


		public HandlerRequestMatcher(final ResourceOwnerPasswordCredentialsGrant grant,
					     final ClientID clientID,
					     final OIDCClientMetadata clientMetadata) {

			this.grant = grant;
			this.clientID = clientID;
			this.clientMetadata = clientMetadata;
		}


		@Override
		public boolean matches(Object o) {

			HandlerRequest handlerRequest;

			try {
				handlerRequest = HandlerRequest.parse(JSONObjectUtils.parse(o.toString()));

			} catch (Exception e) {
				return false;
			}

			if (! grant.getUsername().equals(handlerRequest.getGrant().getUsername()))
				return false;

			if (! grant.getPassword().equals(handlerRequest.getGrant().getPassword()))
				return false;

			if (! clientID.equals(handlerRequest.getClientID()))
				return false;
			
			if (! clientMetadata.toJSONObject().equals(handlerRequest.getClientMetadata().toJSONObject()))
				return false;

			return true;
		}


		@Override
		public void describeTo(Description description) {

		}
	}


	@Before
	public void setUp() {
		initJadler();
	}

	@After
	public void tearDown() {
		
		System.clearProperty("op.grantHandler.password.webAPI.enable");
		System.clearProperty("op.grantHandler.password.webAPI.url");
		System.clearProperty("op.grantHandler.password.webAPI.apiAccessToken");
		System.clearProperty("op.grantHandler.password.webAPI.connectTimeout");
		System.clearProperty("op.grantHandler.password.webAPI.readTimeout");
		
		closeJadler();
	}


	@Test
	public void testGoodRequest()
		throws Exception {

		ResourceOwnerPasswordCredentialsGrant grant = new ResourceOwnerPasswordCredentialsGrant("alice", new Secret("password"));

		Scope scope = Scope.parse("openid");

		ClientID clientID = new ClientID("123");
		OIDCClientMetadata clientMetadata = new OIDCClientMetadata();
		clientMetadata.setGrantTypes(Collections.singleton(GrantType.PASSWORD));

		PasswordGrantAuthorization authzIn = new PasswordGrantAuthorization(
			new Subject("alice"), Scope.parse("openid"), null, false,
			new AccessTokenSpec(0L, null, TokenEncoding.SELF_CONTAINED, Optional.of(false)),
			new RefreshTokenSpec(),
			null);

		onRequest()
			.havingMethodEqualTo("POST")
			.havingBody(new HandlerRequestMatcher(grant, clientID, clientMetadata))
		.respond()
			.withStatus(200)
			.withBody(authzIn.toJSONObject().toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType(ContentType.APPLICATION_JSON.toString());

		InitContext initCtx = new MockInitContext(false, new URL("http://localhost:" + port()));

		PasswordGrantWebAPI api = new PasswordGrantWebAPI();
		api.init(initCtx);
		assertEquals(GrantType.PASSWORD, api.getGrantType());

		PasswordGrantAuthorization authzOut = api.processGrant(grant, scope, clientID, true, clientMetadata);
		assertEquals(authzIn.getSubject(), authzOut.getSubject());
		assertEquals(authzIn.getScope(), authzOut.getScope());
		assertFalse(authzOut.isLongLived());
		assertEquals(0L, authzOut.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authzOut.getAccessTokenSpec().getEncoding());
		assertNull(authzOut.getAccessTokenSpec().getAudience());
		assertFalse(authzOut.getAccessTokenSpec().getEncryptSelfContained().get());
		assertFalse(authzOut.getRefreshTokenSpec().issue());
		assertEquals(-1L, authzOut.getRefreshTokenSpec().getLifetime());
		assertNull(authzOut.getData());
	}


	@Test
	public void testGoodRequest_systemPropertiesConfig()
		throws Exception {

		ResourceOwnerPasswordCredentialsGrant grant = new ResourceOwnerPasswordCredentialsGrant("alice", new Secret("password"));

		Scope scope = Scope.parse("openid");

		ClientID clientID = new ClientID("123");
		OIDCClientMetadata clientMetadata = new OIDCClientMetadata();
		clientMetadata.setGrantTypes(Collections.singleton(GrantType.PASSWORD));

		PasswordGrantAuthorization authzIn = new PasswordGrantAuthorization(
			new Subject("alice"), Scope.parse("openid"), null, false,
			new AccessTokenSpec(0L, null, TokenEncoding.SELF_CONTAINED, Optional.of(false)),
			new RefreshTokenSpec(),
			null);

		onRequest()
			.havingMethodEqualTo("POST")
			.havingBody(new HandlerRequestMatcher(grant, clientID, clientMetadata))
		.respond()
			.withStatus(200)
			.withBody(authzIn.toJSONObject().toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType(ContentType.APPLICATION_JSON.toString());

		URL endpoint = new URL("http://localhost:" + port());
		
		InitContext initCtx = new MockInitContext(true, endpoint);
		
		System.setProperty("op.grantHandler.password.webAPI.enable", "true");
		System.setProperty("op.grantHandler.password.webAPI.url", endpoint.toString());
		System.setProperty("op.grantHandler.password.webAPI.apiAccessToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6");

		PasswordGrantWebAPI api = new PasswordGrantWebAPI();
		api.init(initCtx);
		assertEquals(GrantType.PASSWORD, api.getGrantType());

		PasswordGrantAuthorization authzOut = api.processGrant(grant, scope, clientID, true, clientMetadata);
		assertEquals(authzIn.getSubject(), authzOut.getSubject());
		assertEquals(authzIn.getScope(), authzOut.getScope());
		assertFalse(authzOut.isLongLived());
		assertEquals(0L, authzOut.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authzOut.getAccessTokenSpec().getEncoding());
		assertNull(authzOut.getAccessTokenSpec().getAudience());
		assertFalse(authzOut.getAccessTokenSpec().getEncryptSelfContained().get());
		assertFalse(authzOut.getRefreshTokenSpec().issue());
		assertEquals(-1L, authzOut.getRefreshTokenSpec().getLifetime());
		assertNull(authzOut.getData());
	}


	@Test
	public void testInvalidGrant()
		throws Exception {

		ResourceOwnerPasswordCredentialsGrant grant = new ResourceOwnerPasswordCredentialsGrant("alice", new Secret("password"));

		Scope scope = Scope.parse("openid");

		ClientID clientID = new ClientID("123");
		OIDCClientMetadata clientMetadata = new OIDCClientMetadata();

		onRequest()
			.havingMethodEqualTo("POST")
			.havingBody(new HandlerRequestMatcher(grant, clientID, clientMetadata))
		.respond()
			.withStatus(400)
			.withBody(OAuth2Error.INVALID_GRANT.toJSONObject().toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType(ContentType.APPLICATION_JSON.toString());

		InitContext initCtx = new MockInitContext(false, new URL("http://localhost:" + port()));

		PasswordGrantWebAPI api = new PasswordGrantWebAPI();
		api.init(initCtx);
		assertEquals(GrantType.PASSWORD, api.getGrantType());

		try {
			api.processGrant(grant, scope, clientID, true, clientMetadata);

		} catch (GeneralException e) {

			assertEquals(400, e.getErrorObject().getHTTPStatusCode());
			assertEquals(OAuth2Error.INVALID_GRANT.getCode(), e.getErrorObject().getCode());
			assertEquals(OAuth2Error.INVALID_GRANT.getDescription(), e.getErrorObject().getDescription());
			assertNull(e.getErrorObject().getURI());
		}
	}


	@Test
	public void testProcessHTTP500Response() {

		HTTPResponse httpResponse = new HTTPResponse(HTTPResponse.SC_SERVER_ERROR);

		ErrorObject errorObject = PasswordGrantWebAPI.processNon200Response(httpResponse);
		
		assertEquals(OAuth2Error.SERVER_ERROR, errorObject);
		assertEquals(500, errorObject.getHTTPStatusCode());
	}


	@Test
	public void testProcessHTTP400InvalidGrant() {

		HTTPResponse httpResponse = new HTTPResponse(HTTPResponse.SC_BAD_REQUEST);
		httpResponse.setEntityContentType(ContentType.APPLICATION_JSON);
		httpResponse.setContent(OAuth2Error.INVALID_GRANT.toJSONObject().toJSONString());

		ErrorObject errorObject = PasswordGrantWebAPI.processNon200Response(httpResponse);
		
		assertEquals(OAuth2Error.INVALID_GRANT, errorObject);
		assertEquals(400, errorObject.getHTTPStatusCode());
	}


	@Test
	public void testProcessHTTP400InvalidScope() {

		HTTPResponse httpResponse = new HTTPResponse(HTTPResponse.SC_BAD_REQUEST);
		httpResponse.setEntityContentType(ContentType.APPLICATION_JSON);
		httpResponse.setContent(OAuth2Error.INVALID_SCOPE.toJSONObject().toJSONString());

		ErrorObject errorObject = PasswordGrantWebAPI.processNon200Response(httpResponse);
		
		assertEquals(OAuth2Error.INVALID_SCOPE, errorObject);
		assertEquals(400, errorObject.getHTTPStatusCode());
	}


	@Test
	public void testOverrideEnable()
		throws Exception {

		System.setProperty("op.grantHandler.password.webAPI.enable", "false");

		InitContext initCtx = new MockInitContext(false, new URL("http://localhost:" + port()));

		PasswordGrantWebAPI api = new PasswordGrantWebAPI();
		api.init(initCtx);

		assertFalse(api.isEnabled());
	}


	@Test
	public void testOverrideToken()
		throws Exception {

		BearerAccessToken token = new BearerAccessToken(64);

		System.setProperty("op.grantHandler.password.webAPI.apiAccessToken", token.getValue());

		InitContext initCtx = new MockInitContext(false, new URL("http://localhost:" + port()));

		PasswordGrantWebAPI api = new PasswordGrantWebAPI();
		api.init(initCtx);

		assertEquals(token.getValue(), api.getConfiguration().apiAccessToken.getValue());
	}
}
