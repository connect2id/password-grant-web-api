package com.nimbusds.openid.connect.provider.spi.grants.password.webapi;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Properties;
import javax.servlet.ServletContext;

import org.infinispan.manager.EmbeddedCacheManager;

import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.ServiceContext;


public class MockInitContext implements InitContext {


	private final boolean noResource;
	
	private final URL serviceURL;


	public MockInitContext(final boolean noResource, final URL serviceURL) {

		this.noResource = noResource;
		this.serviceURL = serviceURL;
	}
	
	
	@Override
	public ServletContext getServletContext() {
		return null;
	}
	
	
	@Override
	public InputStream getResourceAsStream(final String path) {
		
		if (noResource) {
			return null;
		}

		Properties props = new Properties();
		props.setProperty("op.grantHandler.password.webAPI.enable", "true");
		props.setProperty("op.grantHandler.password.webAPI.url", serviceURL.toString());
		props.setProperty("op.grantHandler.password.webAPI.apiAccessToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6");
		props.setProperty("op.grantHandler.password.webAPI.connectTimeout", "150");
		props.setProperty("op.grantHandler.password.webAPI.readTimeout", "250");

		ByteArrayOutputStream os = new ByteArrayOutputStream();

		try {
			props.store(os, null);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return new ByteArrayInputStream(os.toByteArray());
	}
	
	
	@Override
	public Issuer getIssuer() {
		return null;
	}
	
	
	@Override
	public Issuer getOPIssuer() {
		return getIssuer();
	}
	
	
	@Override
	public URI getTokenEndpointURI() {
		return null;
	}
	
	
	@Override
	public ServiceContext getServiceContext() {
		return null;
	}
	
	
	@Override
	public EmbeddedCacheManager getInfinispanCacheManager() {
		return null;
	}
}
