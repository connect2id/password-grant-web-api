package com.nimbusds.openid.connect.provider.spi.grants.password.webapi;


import java.util.Properties;

import static org.junit.Assert.*;

import org.junit.Test;


public class ConfigurationTest {


	@Test
	public void testConstructFromProperties() {

		Properties props = new Properties();
		props.setProperty("op.grantHandler.password.webAPI.enable", "true");
		props.setProperty("op.grantHandler.password.webAPI.url", "http://localhost:8080/password-grant-handler");
		props.setProperty("op.grantHandler.password.webAPI.apiAccessToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6");
		props.setProperty("op.grantHandler.password.webAPI.connectTimeout", "150");
		props.setProperty("op.grantHandler.password.webAPI.readTimeout", "250");

		Configuration config = new Configuration(props);

		config.log();

		assertTrue(config.enable);
		assertEquals("http://localhost:8080/password-grant-handler", config.url.toString());
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.apiAccessToken.getValue());
		assertEquals(150, config.connectTimeout);
		assertEquals(250, config.readTimeout);
	}


	@Test
	public void testConstructFromProperties_minimal() {

		Properties props = new Properties();
		props.setProperty("op.grantHandler.password.webAPI.enable", "true");
		props.setProperty("op.grantHandler.password.webAPI.url", "http://localhost:8080/password-grant-handler");
		props.setProperty("op.grantHandler.password.webAPI.apiAccessToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6");

		Configuration config = new Configuration(props);

		config.log();

		assertTrue(config.enable);
		assertEquals("http://localhost:8080/password-grant-handler", config.url.toString());
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.apiAccessToken.getValue());
		assertEquals(0, config.connectTimeout);
		assertEquals(0, config.readTimeout);
	}


	@Test
	public void testDefaults() {
		
		Configuration config = new Configuration(new Properties());
		assertFalse(config.enable);
		assertNull(config.url);
		assertNull(config.apiAccessToken);
		assertEquals(0, config.connectTimeout);
		assertEquals(0, config.readTimeout);
		
		config.log();
	}
}