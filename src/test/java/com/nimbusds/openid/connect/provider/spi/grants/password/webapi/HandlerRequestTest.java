package com.nimbusds.openid.connect.provider.spi.grants.password.webapi;


import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

import net.minidev.json.JSONObject;
import org.junit.Test;

import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.ResourceOwnerPasswordCredentialsGrant;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;


public class HandlerRequestTest {


	@Test
	public void testSerializeAndParse()
		throws Exception {

		ResourceOwnerPasswordCredentialsGrant grant = new ResourceOwnerPasswordCredentialsGrant
			("alice", new Secret("secret"));

		Scope scope = Scope.parse("openid email");

		ClientID clientID = new ClientID("123");

		OIDCClientMetadata clientMetadata = new OIDCClientMetadata();
		clientMetadata.setName("Example app");
		clientMetadata.setGrantTypes(Collections.singleton(GrantType.PASSWORD));

		HandlerRequest handlerRequest = new HandlerRequest(grant, scope, clientID, true, clientMetadata);

		assertEquals(grant, handlerRequest.getGrant());
		assertEquals(clientID, handlerRequest.getClientID());
		assertEquals(clientMetadata, handlerRequest.getClientMetadata());
		assertTrue(handlerRequest.isConfidentialClient());


		JSONObject jsonObject = handlerRequest.toJSONObject();

		System.out.println(jsonObject);

		assertEquals(grant.getUsername(), jsonObject.get("username"));
		assertEquals(grant.getPassword().getValue(), jsonObject.get("password"));
		assertEquals(scope, Scope.parse((List<String>) jsonObject.get("scope")));

		JSONObject clientObject = (JSONObject)jsonObject.get("client");
		assertEquals(clientID.getValue(), clientObject.get("client_id"));
		assertTrue((Boolean)clientObject.get("confidential"));
		assertEquals("Example app", clientObject.get("client_name"));
		assertEquals(Collections.singletonList("password"), clientObject.get("grant_types"));
		assertFalse((Boolean)clientObject.get("require_auth_time"));
		assertEquals(5, clientObject.size());

		assertEquals(4, jsonObject.size());

		handlerRequest = HandlerRequest.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));

		assertEquals(grant.getUsername(), handlerRequest.getGrant().getUsername());
		assertEquals(grant.getPassword().getValue(), handlerRequest.getGrant().getPassword().getValue());
		assertEquals(scope, handlerRequest.getScope());
		assertEquals(clientID, handlerRequest.getClientID());
		assertEquals(clientMetadata.getName(), handlerRequest.getClientMetadata().getName());
		assertTrue(handlerRequest.getClientMetadata().getGrantTypes().contains(GrantType.PASSWORD));
		assertFalse(handlerRequest.getClientMetadata().requiresAuthTime());
	}
}
