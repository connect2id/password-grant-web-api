package com.nimbusds.openid.connect.provider.spi.grants.password.webapi;


import java.net.URL;
import java.util.Properties;

import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nimbusds.common.config.ConfigurationException;
import com.nimbusds.common.config.LoggableConfiguration;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;


/**
 * Resource owner password credentials grant handler configuration. It is
 * typically derived from a Java key / value properties file. The configuration
 * is stored as public fields which become immutable (final) after their
 * initialisation.
 *
 * <p>Example configuration properties:
 *
 * <pre>
 * op.grantHandler.password.webAPI.enable = true
 * op.grantHandler.password.webAPI.url = http://localhost:8080/password-grant-handler
 * op.grantHandler.password.webAPI.apiAccessToken = ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6
 * op.grantHandler.password.webAPI.connectTimeout = 150
 * op.grantHandler.password.webAPI.readTimeout = 250
 * </pre>
 */
public final class Configuration implements LoggableConfiguration {


	/**
	 * The properties prefix.
	 */
	public static final String PREFIX = "op.grantHandler.password.webAPI.";


	/**
	 * Enables / disables the password grant handler.
	 */
	public final boolean enable;


	/**
	 * The endpoint URL of the web API.
	 */
	public final URL url;


	/**
	 * Access token of type Bearer for the web API.
	 */
	public final BearerAccessToken apiAccessToken;


	/**
	 * The HTTP connect timeout, in milliseconds. Zero implies none.
	 */
	public final int connectTimeout;


	/**
	 * The HTTP response read timeout, in milliseconds. Zero implies none.
	 */
	public final int readTimeout;


	/**
	 * Creates a new resource owner password credentials grant handler
	 * configuration from the specified properties.
	 *
	 * @param props The properties. Must not be {@code null}.
	 *
	 * @throws ConfigurationException On a missing or invalid property.
	 */
	public Configuration(final Properties props)
		throws ConfigurationException {

		PropertyRetriever pr = new PropertyRetriever(props, true);

		try {
			enable = pr.getOptBoolean(PREFIX + "enable", false);
			
			if (! enable) {
				url = null;
				apiAccessToken = null;
				connectTimeout = 0;
				readTimeout = 0;
				return;
			}
			
			url = pr.getURL(PREFIX + "url");
			apiAccessToken = new BearerAccessToken(pr.getString(PREFIX + "apiAccessToken"));
			connectTimeout = pr.getOptInt(PREFIX + "connectTimeout", 0);
			readTimeout = pr.getOptInt(PREFIX + "readTimeout", 0);

		} catch (PropertyParseException e) {
			throw new ConfigurationException(e.getMessage() + ": Property: " + e.getPropertyKey());
		}
	}


	/**
	 * Logs the configuration details at INFO level. Properties that may
	 * adversely affect security are logged at WARN level.
	 */
	@Override
	public void log() {

		Logger log = LogManager.getLogger("MAIN");

		log.info("[PGA 0000] Password grant handler web API enabled: {}", enable);
		
		if (! enable) {
			return;
		}
		
		log.info("[PGA 0001] Password grant handler web API URL: {}", url);
		log.info("[PGA 0002] Password grant handler web API HTTP connect timeout: {} ms", connectTimeout);
		log.info("[PGA 0003] Password grant handler web API HTTP read timeout: {} ms", readTimeout);
	}
}
