package com.nimbusds.openid.connect.provider.spi.grants.password.webapi;


import net.minidev.json.JSONObject;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.ResourceOwnerPasswordCredentialsGrant;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;

import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;


/**
 * Encapsulates a request to the password grant web API.
 */
public final class HandlerRequest {


	/**
	 * The Resource Owner Password Credentials grant.
	 */
	private final ResourceOwnerPasswordCredentialsGrant grant;


	/**
	 * The requested scope.
	 */
	private final Scope scope;


	/**
	 * The client identifier.
	 */
	private final ClientID clientID;


	/**
	 * Confidential client flag.
	 */
	private final boolean confidentialClient;


	/**
	 * The client metadata.
	 */
	private final OIDCClientMetadata clientMetadata;


	/**
	 * Creates a new handler request.
	 *
	 * @param grant              The Resource Owner Password Credentials
	 *                           grant. Must not be {@code null}.
	 * @param scope              The requested scope, {@code null} if not
	 *                           specified.
	 * @param clientID           The client identifier. Must not be
	 *                           {@code null}.
	 * @param confidentialClient {@code true} if the client is confidential
	 *                           and has been authenticated, else
	 *                           {@code false}.
	 * @param clientMetadata     The client metadata. Must not be
	 *                           {@code null}.
	 */
	public HandlerRequest(final ResourceOwnerPasswordCredentialsGrant grant,
			      final Scope scope,
			      final ClientID clientID,
			      final boolean confidentialClient,
			      final OIDCClientMetadata clientMetadata) {

		if (grant == null)
			throw new IllegalArgumentException("The grant must not be null");

		this.grant = grant;

		this.scope = scope;

		if (clientID == null)
			throw new IllegalArgumentException("The client ID not be null");
		this.clientID = clientID;

		this.confidentialClient = confidentialClient;

		if (clientMetadata == null)
			throw new IllegalArgumentException("The client metadata must not be null");

		this.clientMetadata = clientMetadata;
	}


	/**
	 * Returns the The Resource Owner Password Credentials grant.
	 *
	 * @return The Resource Owner Password Credentials grant.
	 */
	public ResourceOwnerPasswordCredentialsGrant getGrant() {

		return grant;
	}


	/**
	 * Returns the requested scope.
	 *
	 * @return The scope, {@code null} if not specified.
	 */
	public Scope getScope() {

		return scope;
	}


	/**
	 * Returns the client identifier.
	 *
	 * @return The client identifier.
	 */
	public ClientID getClientID() {

		return clientID;
	}


	/**
	 * Returns the confidential client flag.
	 *
	 * @return {@code true} if the client is confidential and has been
	 *         authenticated, else {@code false}.
	 */
	public boolean isConfidentialClient() {

		return confidentialClient;
	}


	/**
	 * Returns the client metadata.
	 *
	 * @return The client metadata.
	 */
	public OIDCClientMetadata getClientMetadata() {

		return clientMetadata;
	}


	/**
	 * Returns a JSON object representation of this request.
	 *
	 * <p>Example JSON object:
	 *
	 * <pre>
	 * {
	 *   "username" : "alice",
	 *   "password" : "secret",
	 *   "scope"    : [ "openid" , "email" ],
	 *   "client"   : { "client_id"         : "123",
	 *                  "confidential"      : true,
	 *                  "client_name"       : "Example app",
	 *                  "grant_types"       : [ "password" ],
	 *                  "require_auth_time" : false }
	 * }
	 * </pre>
	 *
	 * @return The JSON object.
	 */
	public JSONObject toJSONObject() {

		JSONObject o = new JSONObject();
		o.put("username", grant.getUsername());
		o.put("password", grant.getPassword().getValue());

		if (scope != null) {
			o.put("scope", scope.toStringList());
		}

		JSONObject clientJSONObject = clientMetadata.toJSONObject();

		// Default auth time to false
		if (! clientJSONObject.containsKey("require_auth_time")) {
			clientJSONObject.put("require_auth_time", false);
		}

		clientJSONObject.put("client_id", clientID.getValue());
		clientJSONObject.put("confidential", confidentialClient);
		o.put("client", clientJSONObject);

		return o;
	}


	/**
	 * Parses a handler request from the specified JSON object.
	 *
	 * @param jsonObject The JSON object to parse. Must not be
	 *                   {@code null}.
	 *
	 * @return The handler request.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static HandlerRequest parse(final JSONObject jsonObject)
		throws ParseException {

		String username = JSONObjectUtils.getString(jsonObject, "username");
		Secret password = new Secret(JSONObjectUtils.getString(jsonObject, "password"));

		ResourceOwnerPasswordCredentialsGrant grant = new ResourceOwnerPasswordCredentialsGrant(username, password);

		Scope scope = null;

		if (jsonObject.containsKey("scope")) {
			scope = new Scope(JSONObjectUtils.getStringArray(jsonObject, "scope"));
		}

		JSONObject clientObject = JSONObjectUtils.getJSONObject(jsonObject, "client");

		ClientID clientID = new ClientID(JSONObjectUtils.getString(clientObject, "client_id"));

		boolean confidentialClient = JSONObjectUtils.getBoolean(clientObject, "confidential");

		JSONObject clientMetadataObject = new JSONObject(clientObject);
		clientMetadataObject.remove("client_id");
		clientMetadataObject.remove("confidential");
		
		OIDCClientMetadata clientMetadata = OIDCClientMetadata.parse(clientMetadataObject);

		return new HandlerRequest(grant, scope, clientID, confidentialClient, clientMetadata);
	}
}
