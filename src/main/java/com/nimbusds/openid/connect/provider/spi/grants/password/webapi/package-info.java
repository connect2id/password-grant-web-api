/**
 * Web API for handling resource owner password credentials grants.
 */
package com.nimbusds.openid.connect.provider.spi.grants.password.webapi;
