Password Grant Web API

Copyright (c) Connect2id Ltd., 2014 - 2022


README

Connect2id server handler of resource owner password credentials grants (see
RFC 6749, section 4.3) that delegates processing of the grant to a web service.

See https://connect2id.com/products/server/docs/integration/password-grant-handler

Maven coordinates:

<dependency>
    <groupId>com.nimbusds</groupId>
    <artifactId>oauth-client-grant-web-api</artifactId>
    <version>[version]</version>
</dependency>

where [version] should be the latest stable.

Questions or comments? Email support@connect2id.com
